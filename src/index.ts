declare var firebase:any;

import Vue from 'vue';

import { FirebaseTest } from "./firebasetest";
import { TodoTemplate } from "./todoitem";
import { Item } from "./Item";
import { DefaultFirebaseCallbackHandler } from './DefaultFirebaseCallbackHandler';

const additem = new Vue({
    el: '#add-button',
    methods: {
        addItem: function(itemName) {
            firebasetest.addItem(itemName);
        } 
    }
});

var items = [
]

var itemArea = new Vue({
    el: '#item-area',
    data: {
        itemList: items
    },
    methods: {
        handleDeleteItem: function(item: Item) {
            console.log('handle delete item : ' + item.getContent());
            firebasetest.deleteItem(item.getId());
        }
    }
})

// add item
let db = firebase.firestore();
let firebasetest = new FirebaseTest(db);
let callbackHandler = new DefaultFirebaseCallbackHandler(items);
(new TodoTemplate(firebasetest)).register();
firebasetest.listenItemChange(
    callbackHandler
);
