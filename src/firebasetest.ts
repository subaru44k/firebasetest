import { FirebaseCallbacks } from "./FirebaseCallbacks";
import { Item } from "./Item";

export class FirebaseTest {
    constructor(public db) {
    }

    addItem(item: string) {
        return this.getItemDocumentReference(this.db).add({
            content: item
        });
    }

    deleteItem(docId: string) {
        this.getItemDocumentReference(this.db)
        .doc(docId)
        .delete();
    }

    listenItemChange(callbacks: FirebaseCallbacks) {
        this.getItemDocumentReference(this.db).onSnapshot((querySnapShot) => {
            querySnapShot.docChanges.forEach((change) => {
                if (change.type === 'added') {
                    change.doc.metadata.hasPendingWrites
                     ? callbacks.onLocalItemAdded(new Item(change.doc.id, change.doc.data()['content']))
                     : callbacks.onServerItemAdded(new Item(change.doc.id, change.doc.data()['content']));
                } else if (change.type === 'modified') {
                    change.doc.metadata.hasPendingWrites
                     ? callbacks.onLocalItemChanged(change.doc.data())
                     : callbacks.onServerItemChanged(change.doc.data());
                } else if (change.type === 'removed') {
                    change.doc.metadata.hasPendingWrites
                     ? callbacks.onLocalItemDeleted(new Item(change.doc.id, change.doc.data()['content']))
                     : callbacks.onServerItemDeleted(new Item(change.doc.id, change.doc.data()['content']));
                }  
            });
        });
    }

    private getItemDocumentReference(db) {
        return db.collection("items");
    }
}