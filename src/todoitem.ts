import Vue from 'vue';
import { FirebaseTest } from "./firebasetest";
import { Item } from "./Item";

export class TodoTemplate {
    firebasetest: FirebaseTest;

    templateTag: string = 
          '<div class="card" style="width: 18rem;">'
        + '  <div class="card-body">'
        + '    <h5 class="card-title">{{ todo.getContent() }}</h5>'
        + '    <button v-on:click="deleteItem(todo)">delete</button>'
        + '  </div>'
        + '</div>'; 
 
    constructor(firebasetest) {
        this.firebasetest = firebasetest;
    }
 
    register() {
       Vue.component('todo-item', {
            props: [ 'todo' ],
            template: this.templateTag,
            methods: {
                deleteItem: function(item: Item) {
                    this.$emit('delete-item-event', item);
                }
            }
       });
    }
}