import { FirebaseCallbacks } from "./FirebaseCallbacks";
import { Item } from "./Item";

export class DefaultFirebaseCallbackHandler implements FirebaseCallbacks {
    itemHolder: Item[];

    constructor(itemHolder: Item[]) {
        this.itemHolder = itemHolder;
    }

    onLocalItemAdded(item: Item) {
        this.itemHolder.push(item);
    }
    onLocalItemChanged(item: string) {
        throw new Error("Method not implemented.");
    }
    onLocalItemDeleted(item: Item) {
        this.itemHolder.some(function(v, i) {
            if (v.equals(item)) {
                this.splice(i, 1);
                return true;
            }
            return false;
        }, this.itemHolder);
    }
    onServerItemAdded(item: Item) {
        this.itemHolder.push(item);
    }
    onServerItemChanged(item: string) {
        throw new Error("Method not implemented.");
    }
    onServerItemDeleted(item: Item) {
        this.itemHolder.some(function(v, i) {
            if (v.equals(item)) {
                this.splice(i, 1);
                return true;
            }
            return false;
        }, this.itemHolder);
    }

}