import { Item } from "./Item";

export interface FirebaseCallbacks {
    onLocalItemAdded(item: Item);
    onLocalItemChanged(item: string);
    onLocalItemDeleted(item: Item);
    onServerItemAdded(item: Item);
    onServerItemChanged(item: string);
    onServerItemDeleted(item: Item);
}