module.exports = {
  // メインとなるJavaScriptファイル（エントリーポイント）
  entry: './src/index.ts',
  // ファイルの出力設定
  output: {
    //  出力ファイルのディレクトリ名
    path: `${__dirname}/public/javascripts`,
    // 出力ファイル名
    filename: 'bundle.js'
  },
  module: {
    rules: [
      {
        // 拡張子 .ts の場合
        test: /\.ts$/,
        // TypeScript をコンパイルする
        use: 'awesome-typescript-loader'
      },
      // ソースマップファイルの処理
      {
        enforce: 'pre',
        test: /\.js$/,
        loader: 'source-map-loader'
      }
    ]
  },
  // import 文で .ts ファイルを解決するため
  resolve: {
    extensions: [
      '.ts'
    ],
    alias: {
      vue: 'vue/dist/vue.js',
      auth: 'auth/index.js'
    }

  },
  // ソースマップを有効に
  devtool: 'source-map'
};

